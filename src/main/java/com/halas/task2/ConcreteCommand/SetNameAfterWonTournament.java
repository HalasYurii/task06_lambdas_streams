package com.halas.task2.ConcreteCommand;

import com.halas.task2.Command;
import com.halas.task2.Receiver.Document;

public class SetNameAfterWonTournament implements Command {
    private Document document;
    private String name;

    public SetNameAfterWonTournament(Document doc, String n) {
        this.document = doc;
        this.name = n;
    }

    @Override
    public void execute() {
        document.addName(name+" won tournament, congratulations!");
        document.output();
    }
}
