package com.halas.task2.Invoker;

import com.halas.task2.Command;

import java.util.ArrayList;

public class ManagerCommands {
    private ArrayList<Command> listOfCommands = new ArrayList<>();

    public void addCommand(Command command) {
        listOfCommands.add(command);
    }

    public void run() {
        for (Command command : listOfCommands) {
            command.execute();
        }
    }

}
