package com.halas.task4;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class RunTask4 {
    private static final Scanner input = new Scanner(System.in);

    public static List<String> getWords(String text) {
        List<String> list = new ArrayList<>();

        Pattern p = Pattern.compile("[\\w']+");
        Matcher m = p.matcher(text);
        while (m.find()) {
            list.add(text.substring(m.start(), m.end()));
        }
        return list;
    }

    public static void runFourth() {
        System.out.println("Input your text:");
        String text = input.nextLine();
        List<String> listStr = getWords(text);


        Stream<String> myStream = listStr.stream();

        System.out.println("Number of unique words: "
                + listStr.stream()
                .map(String::toLowerCase)
                .distinct()
                .collect(Collectors.toList())
                .size()
        );

        System.out.println("Sorted list of all unique words: "
                + myStream
                .map(String::toLowerCase)
                .distinct()
                .sorted()
                .collect(Collectors.toList())
        );
        Map<String, Long> countWords = listStr.stream()
                .map(String::toLowerCase)
                .collect(
                        Collectors.groupingBy(String::toString, Collectors.counting())
                );
        System.out.println("Unique words(first version): " + countWords.toString());

        System.out.print("Unique words(second version): {");
        listStr.stream()
                .map(String::toLowerCase)
                .collect(
                        Collectors.groupingBy(String::toString, Collectors.counting())
                )
                .forEach((a, b) -> {
                            if (b == 1) {
                                System.out.print(a + "=" + b + "  ");
                            }
                        }
                );
        System.out.println("}");
    }
}
