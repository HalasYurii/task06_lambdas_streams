package com.halas.task2.ConcreteCommand;

import com.halas.task2.Command;
import com.halas.task2.Receiver.Document;

public class ChangeDocument implements Command {
    private Document document;
    private String newDoc;

    public ChangeDocument(Document document, String newDoc) {
        this.document = document;
        this.newDoc = newDoc;
    }

    @Override
    public void execute() {
        document.ChangeDocument(newDoc);
        document.output();
    }

}
