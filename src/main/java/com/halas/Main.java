package com.halas;

import com.halas.task1.RunTask1;
import com.halas.task2.RunTask2;
import com.halas.task3.RunTask3;
import com.halas.task4.RunTask4;

import java.util.LinkedHashMap;
import java.util.Scanner;

public class Main {
    private LinkedHashMap<String, String> menu;
    private LinkedHashMap<String, FMenu> methodsMenu;
    private static final Scanner input = new Scanner(System.in);

    private Main() {
        initMenu();
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();

        menu.put("1", " 1 - first task");
        menu.put("2", " 2 - second task");
        menu.put("3", " 3 - third task");
        menu.put("4", " 4 - fourth task");
        menu.put("5", " 5 - exit");

        methodsMenu.put("1", RunTask1::runFirst);
        methodsMenu.put("2", RunTask2::runSecond);
        methodsMenu.put("3", RunTask3::runThird);
        methodsMenu.put("4", RunTask4::runFourth);
        methodsMenu.put("5", this::endProgram);
    }

    private void endProgram() {
        System.exit(0);
    }

    private void showMenu() {
        menu.forEach((n, k) -> System.out.println(k));
    }


    public static void main(String[] args) {
        String keyMenu;
        Main main = new Main();

        while (true) {
            try {
                System.out.println("\n\n");
                main.showMenu();
                System.out.println("Please, select menu point.");
                keyMenu = input.nextLine();
                System.out.println("\n\n");
                main.methodsMenu.get(keyMenu).start();
            } catch (Exception e) {
                System.out.println("Error, please try again.\n");
            }
        }
    }
}
