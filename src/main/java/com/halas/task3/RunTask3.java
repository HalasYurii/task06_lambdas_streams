package com.halas.task3;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RunTask3 {
    public static void runThird() {
        final int limit = 20;
        final int start = 5;
        final int end = 100;
        List<Integer> intList =
                new Random()
                        .ints(limit, start, end)
                        .boxed()
                        .sorted()
                        .collect(Collectors.toList());

        System.out.println("Your list: " + intList);

        Stream<Integer> myStream = intList.stream();

        Optional<Integer> max = intList.stream()
                .max(Integer::compareTo);
        System.out.println("Max value: " + max);

        Optional<Integer> sumOne = intList.stream()
                .reduce((a, b) -> a + b);

        System.out.println("Sum with reduce: " + sumOne);

        int sumTwo = intList.stream()
                .mapToInt((a) -> a)
                .sum();

        System.out.println("Sum with .sum: " + sumTwo);

        String stats = intList.stream()
                .mapToInt((a) -> a)
                .summaryStatistics()
                .toString();
        System.out.println("Summary statistics: " + stats);

        OptionalDouble averageOp = intList.stream()
                .mapToInt((a) -> a)
                .average();
        double average = averageOp.isPresent() ? averageOp.getAsDouble() : 0;
        List<Integer> biggerThanAv = myStream
                .filter(a -> a > average)
                .collect(Collectors.toList());

        System.out.println("Numbers(count = " + biggerThanAv.size() + ") that are bigger than average: " + biggerThanAv);
    }
}
