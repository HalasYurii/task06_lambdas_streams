package com.halas.task2.Receiver;

public class Document {
    private String nameDoc = "This is official document!";
    private String titleDoc = "";
    private String endingDoc = "";

    private StringBuilder document;

    public Document() {
        super();
        document = new StringBuilder(nameDoc);
    }

    private void buildDoc() {
        document = new StringBuilder()
                .append(titleDoc)
                .append("\n")
                .append(nameDoc)
                .append("\n")
                .append(endingDoc);
    }

    public void addName(String name) {
        titleDoc = name;
        buildDoc();
    }

    public void ChangeDocument(String c) {
        nameDoc = c;
        buildDoc();
    }

    public void setData(String data) {
        endingDoc = data;
        buildDoc();
    }

    public void output() {
        System.out.println("\nDocument:\n" + document.toString());
    }
}
