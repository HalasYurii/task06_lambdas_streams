package com.halas.task2;

import com.halas.FMenu;
import com.halas.task2.ConcreteCommand.ChangeDocument;
import com.halas.task2.ConcreteCommand.SetData;
import com.halas.task2.ConcreteCommand.SetNameAfterWonTournament;
import com.halas.task2.Invoker.ManagerCommands;
import com.halas.task2.Receiver.Document;

import java.util.LinkedHashMap;
import java.util.Scanner;

public class RunTask2 {

    private LinkedHashMap<String, String> menu;
    private LinkedHashMap<String, FMenu> methodsMenu;
    private static final Scanner input = new Scanner(System.in);
    private static final String defaultEnd = "(if set before, changed it)";


    private Document doc = new Document();

    private RunTask2() {
        initMenu();
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();


        menu.put("1", " 1 - for create new Document");
        menu.put("2", " 2 - set new String to start" + defaultEnd);
        menu.put("3", " 3 - set new String to end" + defaultEnd);
        menu.put("4", " 4 - change name of document");
        menu.put("5", " 5 - get example with interface Command");
        menu.put("6", " 6 - get example with lambdas,ref,anonymous,object");
        menu.put("7", " 7 - exit");

        methodsMenu.put("1", this::oneCreate);
        methodsMenu.put("2", this::twoSetToStart);
        methodsMenu.put("3", this::threeSetToEnd);
        methodsMenu.put("4", this::fourChange);
        methodsMenu.put("5", this::usingCommand);
        methodsMenu.put("6", this::usingNewCommand);
    }

    private void show() {
        menu.forEach((n, k) -> System.out.println(k));
    }

    private void oneCreate() {
        doc = new Document();
        doc.output();
    }

    private void twoSetToStart() {
        System.out.println("Input new string that will be insert on start"
                + defaultEnd);
        String newTitle = input.nextLine();

        doc.addName(newTitle);
        doc.output();
    }

    private void threeSetToEnd() {
        System.out.println("Input new string that will be insert to end"
                + defaultEnd);
        String newEnd = input.nextLine();

        doc.setData(newEnd);
        doc.output();
    }

    private void fourChange() {
        System.out.println("Input new name of document(string): ");
        String newDoc = input.nextLine();

        doc.ChangeDocument(newDoc);
        doc.output();
    }

    private void usingCommand() {
        System.out.println("****Using interface command**** ");

        String name = "Yurii";
        String data = "07.02.2019";
        String newDocument = "This is just paper";

        Document document = new Document();

        Command addName = new SetNameAfterWonTournament(document, name);
        Command addData = new SetData(document, data);
        Command changeDocument = new ChangeDocument(document, newDocument);

        ManagerCommands commands = new ManagerCommands();
        commands.addCommand(addName);
        commands.addCommand(addData);
        commands.addCommand(changeDocument);

        commands.run();
    }

    private void usingNewCommand() {
        System.out.println("****Using: Lambdas, method reference, "
                + "anonymous class, as object of Command interface***");
        String name = "Yurii";
        String newDocument = "This is just paper";

        ManagerCommands commands = new ManagerCommands();

        //as lambda function
        commands.addCommand(() -> {
            doc.addName(name);
            doc.output();
        });

        //as methods reference
        commands.addCommand(this::setData);

        //as anonymous class
        commands.addCommand(new Command() {
            @Override
            public void execute() {
                doc.ChangeDocument(newDocument);
                doc.output();
            }

        });

        //as object interface
        Command objCommand = () -> System.out.println("\nObject interface printed");

        commands.run();
        objCommand.execute();
    }

    private void setData() {
        String data = "07.02.2019";
        doc.setData(data);
        doc.output();
    }


    public static void runSecond() {
        //using interface COMMAND
        String keyMenu = "";
        RunTask2 task2 = new RunTask2();
        while (true) {
            try {
                System.out.println("\n");
                task2.show();
                System.out.println("Please, select menu point.");
                keyMenu = input.nextLine();
                System.out.println("\n\n");
                task2.methodsMenu.get(keyMenu).start();
            } catch (Exception e) {
                if (keyMenu.equals("7")) {
                    break;
                }
                System.out.println("Error, please try again.");
            }
        }
    }
}
