package com.halas.task1;

@FunctionalInterface
public interface Functional {
    Integer func(int a, int b, int c);
}
