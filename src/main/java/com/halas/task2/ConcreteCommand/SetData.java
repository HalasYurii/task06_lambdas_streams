package com.halas.task2.ConcreteCommand;

import com.halas.task2.Command;
import com.halas.task2.Receiver.Document;

public class SetData implements Command {
    private Document document;
    private String data;

    public SetData(Document document, String data) {
        this.document = document;
        this.data = data;
    }

    @Override
    public void execute() {
        document.setData(data);
        document.output();
    }
}
