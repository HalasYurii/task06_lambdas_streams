package com.halas.task2;

//~~Pattern Command~~
@FunctionalInterface
public interface Command {
    void execute();
}
