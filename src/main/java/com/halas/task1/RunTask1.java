package com.halas.task1;

public class RunTask1 {
    public static void runFirst() {
        Functional firstLambda = (a, b, c) -> Integer.max(a, Integer.max(b, c));
        Functional secondLambda = (a, b, c) -> (a + b + c) / 3;

        System.out.println("First lambda, max: " + firstLambda.func(5, 6, 8));
        System.out.println("Second lambda, average: " + secondLambda.func(3, 2, 1));
        System.out.println("\n\n");
    }
}
